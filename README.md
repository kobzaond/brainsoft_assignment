# brainsoft_assignment

## Overview

The application has two parts:
- QA SERVER: This is de-facto the backend of the chatbot. Implemented as Restful, stateless server.
- Client: There are two types of clients: one uses GUI (implemented via streamlit) and one uses CLI

The client allows conversation and file upload (pdf only). If a file is uploaded, it is splitted into chunks, which are than stored to the current vector database. While the server serves several endpoints, to which the client is sending requests. The actual QA is done on the server side, memory is handled by client (although using e.g. langchain integration of Redis would be option too).

The application assumes single-user mode.

Used generative model: OpenAI GPT-3.5-turbo-16k

## Client
- The CLI version is almost identical to the GUI version.
- does not use streaming

```
cd user_interfaces
pip3 install -r requirements.txt
streamlit run bot.py
```

```
cd user_interfaces
pip3 install -r requirements.txt
python3 cli.py
```

*Note, that without running the QA server, client won't work well.

## QA Server:
The server defines few endpoints, the most important ones are for answering and file upload. An example of the usage of these endpoint can be found in user_interfaces/bot.py and user_interfaces/utils.py.

The main server part is written in app.py (fastapi), while the Agent itself can be found in model.py

### Run the server:

```
cd qa_server
pip3 install -r requirements.txt
python3 app.py
```

example of request for qa:

```
response = requests.post(
        url,
        json.dumps({"input": input, 'chat_history': history})).json()
```

### Model
The model defines a class FAISSManager (which uses FAISS vector store). The Manager is using an LLMSingleActionAgent agent (langchain) to do all the work regarding QA. In the first step, the manager makes a memory summary.

In general, the manager takes an input and chat_history, creates a new instance of CustomConversationalRetrieverChain (with the new history) and sends the inputs to the agent.

The agent can use three tools:
 - search 
 - cqa (CustomConversationalRetrieverChain)
 - chitchat

additionally, the agent has its own instance of LLMChain, which is used to decide which tool to use, what steps the agent should do and what to say.

The names of the tools are self-descriptive. The most important tool is the cqa chain (conversationa-question-answering). This chain retrieves k best documents (k=3) and uses GPT to generate an answer. However, it is the agent, not the chain, who generates the final response. Additionally, the cqa chain uses GPT to select relevant references (extract_reference_chain). This is because some documents in the top-3 retrieved ones may not be relevant.

- search - agent can use it to access internet information
- chitchat - is used mainly for chitchating - e.g. "how are you" responses. For such cases, it does not make sense to use tools such as search or cqa.

- data - I used sphinx to generate the data in txt format

- parameters: the data are not splitted into chunks - this is because the documentation is full of codes, and splitting code snipets can result in heavy loss of information. Moreover, with the 16k model, k=3 and max_new_tokens=3k, there cannot be any problem with too long sequences. (if it happened, it is handled by decreasing k). When a pdf file is uploaded, it is splitted into chunks. I use temperature 0 in order to reduce randomness (the agent requires the LLM to 
select a tool by precise commands, higher temperature could therefore result mismatches between LLM output and actual Tool names, which would have negative
result on the performance).

- parsing-error outputs exceptions are handled by making a new chitchat action, where the chitchat tool is instructed to return the final answer.

## Config files
For both, server and client, there are config.yaml files, where one can set api_keys, server uri etc. One must inser his/her own api_keys!, they are not provided by default. Without api_keys, the apps won't be working.

*Note: the agent needs some time to reply, as it calls GPT model several times.



