import json
import requests
import yaml
from http.client import RemoteDisconnected
from urllib3.exceptions import ProtocolError


def call_qa_server(url, input, history, result):
    response = requests.post(
        url,
        json.dumps({"input": input, 'chat_history': history})).json()
    try:
        result['response'] = response['answer']
    except (RemoteDisconnected, ProtocolError, Exception) as e:
        print(response['detail'])
        result['response'] = "Sorry, Something went wrong on the QA \
            server side."
    return result


def truncate_memory(ls, size):
    if len(ls) < size:
        return ls
    return ls[len(ls)-size:]


def transform_history(chat_history):
    return list(
        map(
            lambda x: (
                x[0]['role'] + ": " + x[0]['content'],
                x[1]['role'] + ": " + x[1]['content']
                ),
            zip(
                chat_history[0::2],
                chat_history[1::2]
            )
        )
    )


def get_config():
    with open('config.yaml', 'r') as file:
        config_data = yaml.safe_load(file)
    return config_data
