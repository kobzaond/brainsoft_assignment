import requests
from utils import (
    call_qa_server,
    truncate_memory,
    get_config,
    transform_history
)

config_data = get_config()


def manual():
    print("Welcome to GENAI QA Bot CLI version")
    print("To upload a file, type: :UPLOAD <FILE_PATH>")
    print("To exit, type: :EXIT")


def upload_pdf(file_path):
    try:
        with open(file_path, 'rb') as file:
            resp = requests.post(
                config_data['server_url'] + config_data['upload_pdf_endpoint'],
                params={'filename': file.name},
                files={'file': file}
            )
            print(f"File uploaded with status code: {resp.status_code}")
    except Exception as e:
        print(e)


# Initialize chat history
messages = []


def add_message(role, content):
    messages.append({"role": role, "content": content})


add_message(
    "assistant", "Hello, I am an IBM Python SDK ChatBot, how can I help you?"
    )
# print("Welcome to GENAI QA Bot CLI version")
manual()

# React to user input
while True:
    user_input = input()
    if user_input.startswith(":UPLOAD"):
        file_path = user_input.split(" ")[1]
        upload_pdf(file_path)
    elif user_input == ":EXIT":
        print("Goodbye!")
        break
    else:
        add_message("user", user_input)

        bot_response = None
        result = {'response': None}
        try:
            # thread = threading.Thread(
            print("Wait for it...")
            call_qa_server(
                config_data['server_url'] + config_data['answer_endpoint'],
                user_input,
                transform_history(messages),
                result
            )

            bot_response = result['response']

            if bot_response is not None:
                add_message("assistant", bot_response)
                messages = truncate_memory(messages, config_data['truncation'])
                print(bot_response)
            else:
                print("Sorry, Something went wrong on the QA server side.")
        except:
            print("Sorry, Something went wrong on the QA server side.")
