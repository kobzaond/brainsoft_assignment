import streamlit as st
import requests
import threading
from utils import (
    call_qa_server,
    truncate_memory,
    get_config,
    transform_history
)

config_data = get_config()

st.title("GENAI QA Bot")
upload_pdf = st.file_uploader("Choose a PDF file", type=['pdf'])

if st.button("Upload PDF"):
    resp = requests.post(
        config_data['server_url']+config_data['upload_pdf_endpoint'],
        params={'filename': upload_pdf.name},
        files={'file': upload_pdf.getvalue()}
        )
    if resp.status_code == 200:
        st.write("file uploaded succesfully")
    else:
        print(resp.text)
        st.write("an error occured during file upload")

# Initialize chat history
intro_message = "Hello, I am an IBM Python SDK ChatBot, how can I help you?"
if "messages" not in st.session_state:
    st.session_state.messages = []
    with st.chat_message("assistant"):
        intro_message = "Hello, I am an IBM Python SDK ChatBot, \
            how can I help you?"
        st.markdown("Welcome to GENAI QA Bot")
        st.session_state.messages.append(
            {"role": "assistant", "content": intro_message}
            )

# Display chat messages from history on app rerun
for message in st.session_state.messages:
    with st.chat_message(message["role"]):
        st.markdown(message["content"])

# React to user input
if prompt := st.chat_input("What is up?"):
    # Display user message in chat message container
    with st.chat_message("user"):
        st.markdown(prompt)

    # Add user message to chat history
    st.session_state.messages.append({"role": "user", "content": prompt})

    bot_response = None
    result = {'response': None}
    thread = threading.Thread(
        target=call_qa_server,
        args=(
            config_data['server_url'] + config_data['answer_endpoint'],
            prompt,
            transform_history(st.session_state.messages),
            result
            )
        )

    thread.start()
    with st.spinner('Wait for it...'):
        thread.join()
        bot_response = result['response']

    if bot_response is not None:
        st.session_state.messages.append(
            {"role": "assistant", "content": bot_response}
            )
        st.session_state.messages = truncate_memory(
            st.session_state.messages,
            config_data['truncation']
            )

        # Display assistant response in chat message container
        with st.chat_message("assistant"):
            st.markdown(bot_response)
    else:
        with st.chat_message("assistant"):
            st.markdown("Sorry, Something went wrong on the QA server side.")
