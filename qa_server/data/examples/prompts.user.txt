From all rows in a CSV
**********************

See prompt_from_all_csv.py on GitHub.

   import os
   import pathlib

   from dotenv import load_dotenv

   from genai.credentials import Credentials
   from genai.model import Model
   from genai.prompt_pattern import PromptPattern
   from genai.schemas import GenerateParams

   #
   # In this demo, the following dataset was used:
   #
   # Gorman KB, Williams TD, Fraser WR (2014) "Ecological Sexual Dimorphism and Environmental Variability within a Community of Antarctic Penguins (Genus Pygoscelis)." PLoS ONE 9(3): e90081. doi:10.1371/journal.pone.0090081  # noqa

   # make sure you have a .env file under genai root with
   # GENAI_KEY=<your-genai-key>
   # GENAI_API=<genai-api-endpoint>
   load_dotenv()
   api_key = os.getenv("GENAI_KEY", None)
   api_endpoint = os.getenv("GENAI_API", None)
   PATH = pathlib.Path(__file__).parent.resolve()

   print("\n------------- Example (String Replacement)-------------\n")

   params = GenerateParams(
       decoding_method="greedy",
       max_new_tokens=15,
       min_new_tokens=1,
       stream=False,
   )

   creds = Credentials(api_key, api_endpoint)
   model = Model("google/flan-ul2", params=params, credentials=creds)


   prompt = PromptPattern.from_file(str(PATH) + os.sep + "templates" + os.sep + "synth-animal.yaml")
   print("\nGiven template:\n", prompt)

   prompt.sub("animal", "penguins")
   csv_path = str(PATH) + os.sep + "assets" + os.sep + "penguins.csv"
   mapping = {
       "species": ["species1", "species2", "species3"],
       "island": ["location1", "location2", "location3"],
       "flipper_length_mm": ["length1", "length2", "length3"],
       "year": ["dob1", "dob2", "dob3"],
   }

   list_of_prompts = prompt.sub_all_from_csv(
       csv_path=csv_path,
       col_to_var=mapping,
   )

   print("-----------------------")
   print("generated prompt")
   print(list_of_prompts)
   print(len(list_of_prompts))
   print("-----------------------")


   responses = model.generate_as_completed(list_of_prompts)
   for response in responses:
       print(f"Generated text: {response.generated_text}")


From a random row in a CSV
**************************

See prompt_csv_random_rows.py on GitHub.

   import os
   import pathlib

   from dotenv import load_dotenv

   from genai.credentials import Credentials
   from genai.model import Model
   from genai.prompt_pattern import PromptPattern
   from genai.schemas import GenerateParams

   #
   # In this demo, the following dataset was used:
   #
   # Gorman KB, Williams TD, Fraser WR (2014) "Ecological Sexual Dimorphism and Environmental Variability within a Community of Antarctic Penguins (Genus Pygoscelis)." PLoS ONE 9(3): e90081. doi:10.1371/journal.pone.0090081 # noqa

   # make sure you have a .env file under genai root with
   # GENAI_KEY=<your-genai-key>
   # GENAI_API=<genai-api-endpoint>
   load_dotenv()
   api_key = os.getenv("GENAI_KEY", None)
   api_endpoint = os.getenv("GENAI_API", None)
   PATH = pathlib.Path(__file__).parent.resolve()

   print("\n------------- Example (String Replacement)-------------\n")

   params = GenerateParams(
       decoding_method="greedy",
       max_new_tokens=15,
       min_new_tokens=1,
       stream=False,
   )

   creds = Credentials(api_key, api_endpoint)
   model = Model("google/flan-ul2", params=params, credentials=creds)


   pt = PromptPattern.from_file(str(PATH) + os.sep + "templates" + os.sep + "synth-animal.yaml")
   print("\nGiven template:\n", pt)

   pt.sub("animal", "penguins")
   cvs_path = str(PATH) + os.sep + "assets" + os.sep + "penguins.csv"
   mapping = {
       "species": ["species1", "species2", "species3"],
       "island": ["location1", "location2", "location3"],
       "flipper_length_mm": ["length1", "length2", "length3"],
       "year": ["dob1", "dob2", "dob3"],
   }
   pt.sub_from_csv(csv_path=cvs_path, col_to_var=mapping, strategy="sample")

   print("-----------------------")
   print("generated prompt")
   print(pt)
   print("-----------------------")

   responses = model.generate_as_completed([str(pt)])
   for response in responses:
       print(f"Generated text: {response.generated_text}")


From a JSON
***********

See self_instruct.py on GitHub.

   import os
   import pathlib
   import random

   from genai.prompt_pattern import PromptPattern

   PATH = pathlib.Path(__file__).parent.resolve()

   pt = PromptPattern.from_str(
       """
       Instruction: {{instruction}}
       Input: {{input}}
       Output: {{output}}
   """
   )
   print("\nGiven template:\n", pt)

   json_path = str(PATH) + os.sep + "assets" + os.sep + "seed_tasks.json"

   list_of_prompts = pt.sub_all_from_json(json_path=json_path, key_to_var="infer")

   print("-----------------------")
   print("Generated prompts: \n total number {}".format(len(list_of_prompts)))
   print("Sample prompt: {}".format(list_of_prompts[random.randint(0, len(list_of_prompts) - 1)]))
   print("-----------------------")


From a dataframe
****************

See prompt_from_dataframe.py on GitHub.

   import os
   import pathlib

   try:
       import pandas as pd
   except ImportError:
       raise ImportError("Could not import pandas: Please install ibm-generative-ai[pandas] extension.")

   from dotenv import load_dotenv

   import genai.extensions.pandas  # noqa: F401
   from genai.credentials import Credentials
   from genai.model import Model
   from genai.prompt_pattern import PromptPattern
   from genai.schemas import GenerateParams

   #
   # In this demo, the following dataset was used:
   #
   # Gorman KB, Williams TD, Fraser WR (2014) "Ecological Sexual Dimorphism and Environmental Variability within a Community of Antarctic Penguins (Genus Pygoscelis)." PLoS ONE 9(3): e90081. doi:10.1371/journal.pone.0090081 # noqa

   # make sure you have a .env file under genai root with
   # GENAI_KEY=<your-genai-key>
   # GENAI_API=<genai-api-endpoint>
   load_dotenv()
   api_key = os.getenv("GENAI_KEY", None)
   api_endpoint = os.getenv("GENAI_API", None)
   PATH = pathlib.Path(__file__).parent.resolve()

   print("\n------------- Example (Pandas Dataframe Replacement)-------------\n")

   params = GenerateParams(
       decoding_method="greedy",
       max_new_tokens=15,
       min_new_tokens=1,
       stream=False,
   )

   creds = Credentials(api_key, api_endpoint)
   model = Model("google/flan-ul2", params=params, credentials=creds)


   csv_path = str(PATH) + os.sep + "assets" + os.sep + "penguins.csv"
   df = pd.read_csv(csv_path, index_col=0)

   pt = PromptPattern.from_str("{{species}}: {{island}}, {{flipper_length_mm}}, {{year}}\n")
   list_of_prompts = pt.pandas.sub_from_dataframe(dataframe=df, start_index=15, n=2, strategy="sample")

   print("-----------------------")
   print("generated prompt")
   print(list_of_prompts)
   print("Number of prompts: ", len(list_of_prompts))
   print("-----------------------")

   responses = model.generate_as_completed(list_of_prompts)
   for response in responses:
       print(f"Generated text: {response.generated_text}")


Inferring variables from CSV header
***********************************

See prompt_generation_with_headers.py on GitHub.

   import csv
   import os
   import pathlib
   import random

   import requests

   # Either import genai.extensions.langchain or import specific
   # class from langchain extension for PromptInterface to register
   import genai.extensions.langchain
   from genai.prompt_pattern import PromptPattern

   #
   # Adult dataset in the following example was obtained from UCI Machine
   # Learning repository. The full Bibtex citation is as follows:
   #
   # @misc{Dua:2019 ,
   # author = "Dua, Dheeru and Graff, Casey",
   # year = "2017",
   # title = "{UCI} Machine Learning Repository",
   # url = "http://archive.ics.uci.edu/ml",
   # institution = "University of California, Irvine, School of Information and Computer Sciences" }


   def get_dataset(datafile, headers=None):
       url = "https://archive.ics.uci.edu/ml/machine-learning-databases/adult/adult.data"

       if os.path.exists(datafile):
           print("Data already downloaded. Skipping the download.")
           return
       with requests.get(url) as response:
           print("Saving data to {}".format(datafile))
           # Add header
           if headers is not None:
               with open(datafile, "w", newline="") as f:
                   csv.writer(f).writerow(headers)
           # Add data
           with open(datafile, "ab") as f:
               f.write(response.content)


   headers = [
       "age",
       "workclass",
       "demographic",
       "education_str",
       "education",
       "marital_status",
       "occupation",
       "relationship_status",
       "race",
       "sex",
       "capital_gain",
       "capital_loss",
       "work_hours_per_week",
       "native_country",
       "salary",
   ]
   datafile = os.path.join(pathlib.Path(__file__).parent.absolute(), "adult.header.data.csv")
   get_dataset(datafile, headers=headers)

   # flake8: noqa
   pattern = """
       The age of the individual is {{age}}.
       The workclass of the individual is {{workclass}}.
       The estimate of the number of individuals in the population with the same demographics as the individual was {{demographic}}.
       The numeric form of the highest education level they achieved was {{education}}.
       The string form of the highest education level they achieved was {{education_str}}.
       The marital status of the individual was {{marital_status}}.
       The occupation of the individual was {{occupation}}.
       The relationship status of the individual is {{relationship_status}}.
       The race of the individual was {{race}}.
       The sex of the individual is {{sex}}.
       The capital gain they had in the previous year was {{capital_gain}}.
       The capital loss they had in the previous year was {{capital_loss}}.
       The number of hours worked per week was {{work_hours_per_week}}.
       The native country of the individual was {{native_country}}.
       The salary of this individual was {{salary}}.
   """


   prompt_pattern = PromptPattern.from_str(pattern)

   print("\nGiven pattern:\n", prompt_pattern)

   prompts = prompt_pattern.sub_all_from_csv(csv_path=datafile, col_to_var="infer")

   print("-----------------------")
   print("Generated prompts: \n total number {}".format(len(prompts)))
   print("Sample prompt: {}".format(prompts[random.randint(0, len(prompts) - 1)]))
   print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

   # Langchain raw string templates are created as
   # PromptTemplate(input_vars=[variable list], template=raw_string).
   # In the following example, this template gets created automatically
   # from genai PromptPattern using langchain extension.
   # Langchain PromptTemplates are substituted with variable values as
   # template.format(variable1=val1, variable2=val2, ....)
   # In the following example, when we read csv using DictReader, each
   # line gets read as a dictionary {column1: val, column2: val, ...}.
   # We use ** operator in python to feed this dictionary to format
   # the langchain template as template.format(**line)

   print("Translating prompt pattern to langchain template")
   template = prompt_pattern.langchain.as_template()
   print("Generated langchain template = ", template)
   prompts = []
   with open(datafile, "r") as fin:
       reader = csv.DictReader(fin)
       for line in reader:
           if len(line) < len(headers):
               continue
           prompts.append(template.format(**line))  # **operator converts dict into key-val args

   print("-----------------------")
   print("Generated prompts: \n total number {}".format(len(prompts)))
   print("Sample prompt: {}".format(prompts[random.randint(0, len(prompts) - 1)]))
   print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

   # In the following snippet, we create a genai PromptPattern from
   # a langchain PromptTemplate using langchain extension of genai.
   print("Translating langchain template back to prompt pattern")
   prompt_pattern = PromptPattern.langchain.from_template(template)
   prompts = prompt_pattern.sub_all_from_csv(csv_path=datafile, col_to_var="infer")
   print("-----------------------")
   print("Generated prompts: \n total number {}".format(len(prompts)))
   print("Sample prompt: {}".format(prompts[random.randint(0, len(prompts) - 1)]))
   print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
