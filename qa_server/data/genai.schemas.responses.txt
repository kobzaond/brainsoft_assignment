Responses
*********

class genai.schemas.responses.ErrorExtensionState(*, instancePath: str, params: ErrorExtensionStateParam, **extra_data: Any)

   Bases: "GenAiResponseModel"

   instancePath: str

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'instancePath': FieldInfo(annotation=str, required=True), 'params': FieldInfo(annotation=ErrorExtensionStateParam, required=True)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   params: ErrorExtensionStateParam

class genai.schemas.responses.ErrorExtensionStateParam(*, comparison: str, limit: int, **extra_data: Any)

   Bases: "GenAiResponseModel"

   comparison: str

   limit: int

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'comparison': FieldInfo(annotation=str, required=True), 'limit': FieldInfo(annotation=int, required=True)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

class genai.schemas.responses.ErrorExtensions(*, code: str, state: list[genai.schemas.responses.ErrorExtensionState], **extra_data: Any)

   Bases: "GenAiResponseModel"

   code: str

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'code': FieldInfo(annotation=str, required=True), 'state': FieldInfo(annotation=list[ErrorExtensionState], required=True)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   state: list[genai.schemas.responses.ErrorExtensionState]

class genai.schemas.responses.ErrorResponse(*, status_code: int, error: str, message: str, extensions: Optional[ErrorExtensions] = None, **extra_data: Any)

   Bases: "GenAiResponseModel"

   error: str

   extensions: Optional[ErrorExtensions]

   message: str

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'error': FieldInfo(annotation=str, required=True), 'extensions': FieldInfo(annotation=Union[ErrorExtensions, NoneType], required=False), 'message': FieldInfo(annotation=str, required=True), 'status_code': FieldInfo(annotation=int, required=True)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   status_code: int

class genai.schemas.responses.FileFormatResult(*, id: int, name: str, **extra_data: Any)

   Bases: "GenAiResponseModel"

   id: int

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'id': FieldInfo(annotation=int, required=True), 'name': FieldInfo(annotation=str, required=True)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   name: str

class genai.schemas.responses.FileInfoResult(*, id: str, bytes: int, file_name: str, purpose: str, storage_provider_location: Optional[str] = None, created_at: datetime, file_formats: List[FileFormatResult], **extra_data: Any)

   Bases: "GenAiResponseModel"

   bytes: int

   created_at: datetime

   file_formats: List[FileFormatResult]

   file_name: str

   id: str

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'bytes': FieldInfo(annotation=int, required=True), 'created_at': FieldInfo(annotation=datetime, required=True), 'file_formats': FieldInfo(annotation=List[FileFormatResult], required=True), 'file_name': FieldInfo(annotation=str, required=True), 'id': FieldInfo(annotation=str, required=True), 'purpose': FieldInfo(annotation=str, required=True), 'storage_provider_location': FieldInfo(annotation=Union[str, NoneType], required=False)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   purpose: str

   storage_provider_location: Optional[str]

class genai.schemas.responses.FilesListResponse(*, results: List[FileInfoResult], totalCount: int, **extra_data: Any)

   Bases: "GenAiResponseModel"

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'results': FieldInfo(annotation=List[FileInfoResult], required=True), 'totalCount': FieldInfo(annotation=int, required=True)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   results: List[FileInfoResult]

   totalCount: int

class genai.schemas.responses.GenAiResponseModel(**extra_data: Any)

   Bases: "BaseModel"

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

class genai.schemas.responses.GenerateLimits(*, tokenCapacity: int, tokensUsed: int, **extra_data: Any)

   Bases: "GenAiResponseModel"

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'tokenCapacity': FieldInfo(annotation=int, required=True), 'tokensUsed': FieldInfo(annotation=int, required=True)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   tokenCapacity: int

   tokensUsed: int

class genai.schemas.responses.GenerateResponse(*, id: str, model_id: str, created_at: str, results: List[GenerateResult], **extra_data: Any)

   Bases: "GenAiResponseModel"

   created_at: str

   id: str

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'created_at': FieldInfo(annotation=str, required=True), 'id': FieldInfo(annotation=str, required=True), 'model_id': FieldInfo(annotation=str, required=True), 'results': FieldInfo(annotation=List[GenerateResult], required=True)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   model_id: str

   results: List[GenerateResult]

class genai.schemas.responses.GenerateResult(*, generated_text: str, generated_token_count: int, input_token_count: Optional[int] = None, stop_reason: str, stop_sequence: Optional[str] = None, generated_tokens: Optional[list[genai.schemas.responses.GeneratedToken]] = None, input_text: Optional[str] = None, seed: Optional[int] = None, moderation: Optional[ModerationResult] = None, **extra_data: Any)

   Bases: "GenAiResponseModel"

   generated_text: str

   generated_token_count: int

   generated_tokens: Optional[list[genai.schemas.responses.GeneratedToken]]

   input_text: Optional[str]

   input_token_count: Optional[int]

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'generated_text': FieldInfo(annotation=str, required=True), 'generated_token_count': FieldInfo(annotation=int, required=True), 'generated_tokens': FieldInfo(annotation=Union[list[GeneratedToken], NoneType], required=False), 'input_text': FieldInfo(annotation=Union[str, NoneType], required=False), 'input_token_count': FieldInfo(annotation=Union[int, NoneType], required=False), 'moderation': FieldInfo(annotation=Union[ModerationResult, NoneType], required=False), 'seed': FieldInfo(annotation=Union[int, NoneType], required=False), 'stop_reason': FieldInfo(annotation=str, required=True), 'stop_sequence': FieldInfo(annotation=Union[str, NoneType], required=False)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   moderation: Optional[ModerationResult]

   seed: Optional[int]

   stop_reason: str

   stop_sequence: Optional[str]

class genai.schemas.responses.GenerateStreamResponse(*, id: str, model_id: str, created_at: str, results: List[GenerateStreamResult], **extra_data: Any)

   Bases: "GenAiResponseModel"

   created_at: str

   id: str

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'created_at': FieldInfo(annotation=str, required=True), 'id': FieldInfo(annotation=str, required=True), 'model_id': FieldInfo(annotation=str, required=True), 'results': FieldInfo(annotation=List[GenerateStreamResult], required=True)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   model_id: str

   results: List[GenerateStreamResult]

class genai.schemas.responses.GenerateStreamResult(*, generated_text: Optional[str] = None, generated_token_count: Optional[int] = None, input_token_count: Optional[int] = None, stop_reason: Optional[str] = None, generated_tokens: Optional[list[genai.schemas.responses.GeneratedToken]] = None, input_text: Optional[str] = None, seed: Optional[int] = None, moderation: Optional[ModerationResult] = None, **extra_data: Any)

   Bases: "GenAiResponseModel"

   generated_text: Optional[str]

   generated_token_count: Optional[int]

   generated_tokens: Optional[list[genai.schemas.responses.GeneratedToken]]

   input_text: Optional[str]

   input_token_count: Optional[int]

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'generated_text': FieldInfo(annotation=Union[str, NoneType], required=False), 'generated_token_count': FieldInfo(annotation=Union[int, NoneType], required=False), 'generated_tokens': FieldInfo(annotation=Union[list[GeneratedToken], NoneType], required=False), 'input_text': FieldInfo(annotation=Union[str, NoneType], required=False), 'input_token_count': FieldInfo(annotation=Union[int, NoneType], required=False), 'moderation': FieldInfo(annotation=Union[ModerationResult, NoneType], required=False), 'seed': FieldInfo(annotation=Union[int, NoneType], required=False), 'stop_reason': FieldInfo(annotation=Union[str, NoneType], required=False)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   moderation: Optional[ModerationResult]

   seed: Optional[int]

   stop_reason: Optional[str]

class genai.schemas.responses.GeneratedToken(*, logprob: Optional[float] = None, text: Optional[str] = None, **extra_data: Any)

   Bases: "GenAiResponseModel"

   logprob: Optional[float]

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'logprob': FieldInfo(annotation=Union[float, NoneType], required=False), 'text': FieldInfo(annotation=Union[str, NoneType], required=False)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   text: Optional[str]

class genai.schemas.responses.HAPResult(*, flagged: bool, score: float, success: bool, position: TextPosition, **extra_data: Any)

   Bases: "GenAiResponseModel"

   flagged: bool

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'flagged': FieldInfo(annotation=bool, required=True), 'position': FieldInfo(annotation=TextPosition, required=True), 'score': FieldInfo(annotation=float, required=True), 'success': FieldInfo(annotation=bool, required=True)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   position: TextPosition

   score: float

   success: bool

class genai.schemas.responses.HistoryResponse(*, results: List[HistoryResult], totalCount: int, **extra_data: Any)

   Bases: "GenAiResponseModel"

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'results': FieldInfo(annotation=List[HistoryResult], required=True), 'totalCount': FieldInfo(annotation=int, required=True)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   results: List[HistoryResult]

   totalCount: int

class genai.schemas.responses.HistoryResult(*, id: str, duration: int, request: HistoryResultRequest, status: str, created_at: datetime, response: Union[GenerateResponse, Any], **extra_data: Any)

   Bases: "GenAiResponseModel"

   created_at: datetime

   duration: int

   id: str

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'created_at': FieldInfo(annotation=datetime, required=True), 'duration': FieldInfo(annotation=int, required=True), 'id': FieldInfo(annotation=str, required=True), 'request': FieldInfo(annotation=HistoryResultRequest, required=True), 'response': FieldInfo(annotation=Union[GenerateResponse, Any], required=True), 'status': FieldInfo(annotation=str, required=True)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   request: HistoryResultRequest

   response: Union[GenerateResponse, Any]

   status: str

class genai.schemas.responses.HistoryResultRequest(*, inputs: list[str], model_id: str, parameters: GenerateParams, **extra_data: Any)

   Bases: "GenAiResponseModel"

   inputs: list[str]

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'inputs': FieldInfo(annotation=list[str], required=True), 'model_id': FieldInfo(annotation=str, required=True), 'parameters': FieldInfo(annotation=GenerateParams, required=True)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   model_id: str

   parameters: GenerateParams

class genai.schemas.responses.ModelCard(*, id: Optional[str] = None, name: Optional[str] = None, size: Optional[str] = None, source_model_id: Optional[str] = None, token_limit: Optional[Union[int, Any]] = None, **extra_data: Any)

   Bases: "GenAiResponseModel"

   id: Optional[str]

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'id': FieldInfo(annotation=Union[str, NoneType], required=False), 'name': FieldInfo(annotation=Union[str, NoneType], required=False), 'size': FieldInfo(annotation=Union[str, NoneType], required=False), 'source_model_id': FieldInfo(annotation=Union[str, NoneType], required=False), 'token_limit': FieldInfo(annotation=Union[int, Any, NoneType], required=False)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   name: Optional[str]

   size: Optional[str]

   source_model_id: Optional[str]

   token_limit: Optional[Union[int, Any]]

class genai.schemas.responses.ModelList(*, results: list[genai.schemas.responses.ModelCard], **extra_data: Any)

   Bases: "GenAiResponseModel"

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'results': FieldInfo(annotation=list[ModelCard], required=True)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   results: list[genai.schemas.responses.ModelCard]

class genai.schemas.responses.ModerationResult(*, hap: List[HAPResult], **extra_data: Any)

   Bases: "GenAiResponseModel"

   hap: List[HAPResult]

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'hap': FieldInfo(annotation=List[HAPResult], required=True)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

class genai.schemas.responses.StopReasonEnum(value, names=None, *, module=None, qualname=None, type=None, start=1, boundary=None)

   Bases: "str", "Enum"

   CANCELLED = 'Request canceled by the client'

   EOS_TOKEN = 'End of sequence token encountered'

   ERROR = 'Error encountered'

   MAX_TOKENS = 'Maximum requested tokens reached'

   NOT_FINISHED = 'Possibly more tokens to be streamed'

   STOP_SEQUENCE = 'Stop sequence encountered'

   TIME_LIMIT = 'Time limit reached'

   TOKEN_LIMIT = 'Token limit reached'

class genai.schemas.responses.TermsOfUse(*, results: TermsOfUseResult, **extra_data: Any)

   Bases: "GenAiResponseModel"

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'results': FieldInfo(annotation=TermsOfUseResult, required=True)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   results: TermsOfUseResult

class genai.schemas.responses.TermsOfUseResult(*, tou_accepted: bool, tou_accepted_at: datetime, firstName: str, lastName: str, data_usage_consent: bool = False, generate_default: Optional[dict] = None, **extra_data: Any)

   Bases: "GenAiResponseModel"

   data_usage_consent: bool

   firstName: str

   generate_default: Optional[dict]

   lastName: str

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'data_usage_consent': FieldInfo(annotation=bool, required=False, default=False), 'firstName': FieldInfo(annotation=str, required=True), 'generate_default': FieldInfo(annotation=Union[dict, NoneType], required=False), 'lastName': FieldInfo(annotation=str, required=True), 'tou_accepted': FieldInfo(annotation=bool, required=True), 'tou_accepted_at': FieldInfo(annotation=datetime, required=True)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   tou_accepted: bool

   tou_accepted_at: datetime

class genai.schemas.responses.TextPosition(*, start: int, end: int, **extra_data: Any)

   Bases: "GenAiResponseModel"

   end: int

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'end': FieldInfo(annotation=int, required=True), 'start': FieldInfo(annotation=int, required=True)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   start: int

class genai.schemas.responses.TokenizeResponse(*, model_id: str, created_at: str, results: List[TokenizeResult], **extra_data: Any)

   Bases: "GenAiResponseModel"

   created_at: str

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'created_at': FieldInfo(annotation=str, required=True), 'model_id': FieldInfo(annotation=str, required=True), 'results': FieldInfo(annotation=List[TokenizeResult], required=True)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   model_id: str

   results: List[TokenizeResult]

class genai.schemas.responses.TokenizeResult(*, token_count: Optional[int] = None, tokens: Optional[List[str]] = None, input_text: Optional[str] = None, **extra_data: Any)

   Bases: "GenAiResponseModel"

   input_text: Optional[str]

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'input_text': FieldInfo(annotation=Union[str, NoneType], required=False), 'token_count': FieldInfo(annotation=Union[int, NoneType], required=False), 'tokens': FieldInfo(annotation=Union[List[str], NoneType], required=False)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   token_count: Optional[int]

   tokens: Optional[List[str]]

class genai.schemas.responses.TrainingFilesParameters(*, id: str, file_name: str, created_at: str, **extra_data: Any)

   Bases: "GenAiResponseModel"

   created_at: str

   file_name: str

   id: str

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'created_at': FieldInfo(annotation=str, required=True), 'file_name': FieldInfo(annotation=str, required=True), 'id': FieldInfo(annotation=str, required=True)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

class genai.schemas.responses.TuneGetResponse(*, results: Optional[TuneInfoResult] = None, **extra_data: Any)

   Bases: "GenAiResponseModel"

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'results': FieldInfo(annotation=Union[TuneInfoResult, NoneType], required=False)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   results: Optional[TuneInfoResult]

class genai.schemas.responses.TuneInfoResult(*, id: str, name: str, model_id: str, model_name: str, method_id: Optional[str] = None, method_name: Optional[str] = None, status: str, task_id: str, task_name: Optional[str] = None, parameters: Optional[TuneParameters] = None, created_at: datetime, preferred: Optional[bool] = None, datapoints: Optional[dict] = None, validation_files: Optional[list] = None, training_files: Optional[list] = None, evaluation_files: Optional[list] = None, status_message: Optional[str] = None, started_at: Optional[datetime] = None, **extra_data: Any)

   Bases: "GenAiResponseModel"

   created_at: datetime

   datapoints: Optional[dict]

   evaluation_files: Optional[list]

   id: str

   method_id: Optional[str]

   method_name: Optional[str]

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'created_at': FieldInfo(annotation=datetime, required=True), 'datapoints': FieldInfo(annotation=Union[dict, NoneType], required=False), 'evaluation_files': FieldInfo(annotation=Union[list, NoneType], required=False), 'id': FieldInfo(annotation=str, required=True), 'method_id': FieldInfo(annotation=Union[str, NoneType], required=False), 'method_name': FieldInfo(annotation=Union[str, NoneType], required=False), 'model_id': FieldInfo(annotation=str, required=True), 'model_name': FieldInfo(annotation=str, required=True), 'name': FieldInfo(annotation=str, required=True), 'parameters': FieldInfo(annotation=Union[TuneParameters, NoneType], required=False), 'preferred': FieldInfo(annotation=Union[bool, NoneType], required=False), 'started_at': FieldInfo(annotation=Union[datetime, NoneType], required=False), 'status': FieldInfo(annotation=str, required=True), 'status_message': FieldInfo(annotation=Union[str, NoneType], required=False), 'task_id': FieldInfo(annotation=str, required=True), 'task_name': FieldInfo(annotation=Union[str, NoneType], required=False), 'training_files': FieldInfo(annotation=Union[list, NoneType], required=False), 'validation_files': FieldInfo(annotation=Union[list, NoneType], required=False)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   model_id: str

   model_name: str

   name: str

   parameters: Optional[TuneParameters]

   preferred: Optional[bool]

   started_at: Optional[datetime]

   status: str

   status_message: Optional[str]

   task_id: str

   task_name: Optional[str]

   training_files: Optional[list]

   validation_files: Optional[list]

class genai.schemas.responses.TuneMethodsGetResponse(*, results: Optional[List[TuneMethodsInfo]] = None, **extra_data: Any)

   Bases: "GenAiResponseModel"

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'results': FieldInfo(annotation=Union[List[TuneMethodsInfo], NoneType], required=False)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   results: Optional[List[TuneMethodsInfo]]

class genai.schemas.responses.TuneMethodsInfo(*, id: str, name: str, **extra_data: Any)

   Bases: "GenAiResponseModel"

   id: str

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'id': FieldInfo(annotation=str, required=True), 'name': FieldInfo(annotation=str, required=True)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   name: str

class genai.schemas.responses.TuneParameters(*, accumulate_steps: Optional[int] = None, batch_size: Optional[int] = None, learning_rate: Optional[float] = None, max_input_tokens: Optional[int] = None, max_output_tokens: Optional[int] = None, num_epochs: Optional[int] = None, num_virtual_tokens: Optional[int] = None, verbalizer: Optional[str] = None, **extra_data: Any)

   Bases: "GenAiResponseModel"

   accumulate_steps: Optional[int]

   batch_size: Optional[int]

   learning_rate: Optional[float]

   max_input_tokens: Optional[int]

   max_output_tokens: Optional[int]

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'accumulate_steps': FieldInfo(annotation=Union[int, NoneType], required=False), 'batch_size': FieldInfo(annotation=Union[int, NoneType], required=False), 'learning_rate': FieldInfo(annotation=Union[float, NoneType], required=False), 'max_input_tokens': FieldInfo(annotation=Union[int, NoneType], required=False), 'max_output_tokens': FieldInfo(annotation=Union[int, NoneType], required=False), 'num_epochs': FieldInfo(annotation=Union[int, NoneType], required=False), 'num_virtual_tokens': FieldInfo(annotation=Union[int, NoneType], required=False), 'verbalizer': FieldInfo(annotation=Union[str, NoneType], required=False)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   num_epochs: Optional[int]

   num_virtual_tokens: Optional[int]

   verbalizer: Optional[str]

class genai.schemas.responses.TunesListResponse(*, results: List[TuneInfoResult], totalCount: int, **extra_data: Any)

   Bases: "GenAiResponseModel"

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'results': FieldInfo(annotation=List[TuneInfoResult], required=True), 'totalCount': FieldInfo(annotation=int, required=True)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   results: List[TuneInfoResult]

   totalCount: int

class genai.schemas.responses.WatsonxRenderedPrompts(*, results: list[str], **extra_data: Any)

   Bases: "GenAiResponseModel"

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'results': FieldInfo(annotation=list[str], required=True)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   results: list[str]

class genai.schemas.responses.WatsonxTemplate(*, id: str, name: str, value: str, created_at: datetime, data: Optional[dict] = None, **extra_data: Any)

   Bases: "GenAiResponseModel"

   created_at: datetime

   data: Optional[dict]

   id: str

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'created_at': FieldInfo(annotation=datetime, required=True), 'data': FieldInfo(annotation=Union[dict, NoneType], required=False), 'id': FieldInfo(annotation=str, required=True), 'name': FieldInfo(annotation=str, required=True), 'value': FieldInfo(annotation=str, required=True)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   name: str

   value: str

class genai.schemas.responses.WatsonxTemplatesResponse(*, results: list[genai.schemas.responses.WatsonxTemplate], totalCount: int, **extra_data: Any)

   Bases: "GenAiResponseModel"

   model_config: ClassVar[ConfigDict] = {'extra': 'allow', 'protected_namespaces': ()}

      Configuration for the model, should be a dictionary conforming
      to [*ConfigDict*][pydantic.config.ConfigDict].

   model_fields: ClassVar[dict[str, FieldInfo]] = {'results': FieldInfo(annotation=list[WatsonxTemplate], required=True), 'totalCount': FieldInfo(annotation=int, required=True)}

      Metadata about the fields defined on the model, mapping of field
      names to [*FieldInfo*][pydantic.fields.FieldInfo].

      This replaces *Model.__fields__* from Pydantic V1.

   results: list[genai.schemas.responses.WatsonxTemplate]

   totalCount: int

genai.schemas.responses.alert_extra_fields_validator(cls: Type[BaseModel], values) -> dict

   Reusable validator that logs a warning if we see undocmented fields
   returned by the API

   Parameters:
      * **cls** (*ModelMetaclass*) -- the pydantic model that is being
        validated

      * **values** (*dict*) -- The incoming values to be checkec

   Returns:
      The values post validation

   Return type:
      dict
