from langchain.text_splitter import RecursiveCharacterTextSplitter
from pathlib import Path
import os
from langchain.vectorstores import FAISS
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.chains.conversational_retrieval.base import (
    ConversationalRetrievalChain
)
from iteration_utilities import flatten
from langchain.document_loaders import PyMuPDFLoader
import re
from langchain.document_loaders import TextLoader
from langchain.agents import (
    Tool,
    AgentExecutor,
    LLMSingleActionAgent,
    AgentOutputParser
)
from langchain.prompts import BaseChatPromptTemplate
from langchain.utilities import SerpAPIWrapper
from langchain.chains.llm import LLMChain
from langchain.chat_models import ChatOpenAI
from langchain.schema import AgentAction, AgentFinish, HumanMessage
from langchain.agents import tool
from langchain.callbacks.manager import (
    CallbackManagerForChainRun,
    Callbacks,
)
from typing import Any, Dict, List, Optional, Union
import inspect
from langchain.chains.conversational_retrieval.prompts import (
    CONDENSE_QUESTION_PROMPT
)
from langchain.schema.language_model import BaseLanguageModel
from langchain.schema import BasePromptTemplate, BaseRetriever
from langchain.chains.question_answering import load_qa_chain
from langchain.prompts import PromptTemplate
import yaml


def extract_int_lists(text):
    # Define the pattern for matching lists of integers
    pattern = r'\[([\d,\s]+)\]'

    # Find all matches using the pattern
    matches = re.findall(pattern, text)

    # Convert each match to a list of integers
    int_lists = []
    for match in matches:
        int_list = [int(num) for num in match.replace(',', '').split()]
        int_lists.append(int_list)

    return int_lists


def loader():
    def list_files(directory):
        file_list = []
        for root, directories, files in os.walk(directory):
            for filename in files:
                file_list.append(os.path.join(root, filename))
        return file_list

    onlyfiles = list_files('data/')
    docs = list(flatten(list(map(lambda x: TextLoader(x).load(), onlyfiles))))

    return docs


# Set up a prompt template
class CustomPromptTemplate(BaseChatPromptTemplate):
    # The template to use
    template: str
    # The list of tools available
    tools: List[Tool]

    def format_messages(self, **kwargs) -> str:
        # Get the intermediate steps (AgentAction, Observation tuples)
        # Format them in a particular way
        intermediate_steps = kwargs.pop("intermediate_steps")
        thoughts = ""
        for action, observation in intermediate_steps:
            thoughts += action.log
            thoughts += f"\nObservation: {observation}\nThought: "
        # Set the agent_scratchpad variable to that value
        kwargs["agent_scratchpad"] = thoughts
        # Create a tools variable from the list of tools provided
        kwargs["tools"] = "\n".join(
            [f"{tool.name}: {tool.description}" for tool in self.tools]
            )
        # Create a list of tool names for the tools provided
        kwargs["tool_names"] = ", ".join([tool.name for tool in self.tools])
        formatted = self.template.format(**kwargs)
        return [HumanMessage(content=formatted)]


class CustomOutputParser(AgentOutputParser):

    def parse(self, llm_output: str) -> Union[AgentAction, AgentFinish]:
        # Check if agent should finish
        if "Final Answer:" in llm_output:
            return AgentFinish(
                # Return values is generally always a
                # dictionary with a single `output` key
                # It is not recommended to
                # try anything else at the moment :)
                return_values={
                    "output": llm_output.split("Final Answer:")[-1].strip()
                    },
                log=llm_output,
            )
        # Parse out the action and action input
        regex = r"Action\s*\d*\s*:(.*?)\nAction\s*\d*\s*Input\s*\d*\s*:[\s]*(.*)"
        match = re.search(regex, llm_output, re.DOTALL)
        if not match:
            print("error!!!!!!!!!")
            return AgentAction(
                tool='Chitchat',
                tool_input=llm_output + '\n\nThis must be the Final Answer',
                log=llm_output
                )
        action = match.group(1).strip()
        action_input = match.group(2)
        # Return the action and action input
        return AgentAction(
            tool=action,
            tool_input=action_input.strip(" ").strip('"'),
            log=llm_output
            )


class CustomConversationalRetrievalChain(ConversationalRetrievalChain):

    extract_reference_chain: LLMChain
    chat_history: str

    @property
    def input_keys(self) -> List[str]:
        """Input keys."""
        return ["question"]

    def _call(
        self,
        inputs: Dict[str, Any],
        run_manager: Optional[CallbackManagerForChainRun] = None,
    ) -> Dict[str, Any]:
        _run_manager = run_manager or CallbackManagerForChainRun.get_noop_manager()
        question = inputs["question"]
        chat_history_str = self.chat_history

        if chat_history_str:
            callbacks = _run_manager.get_child()
            new_question = self.question_generator.run(
                question=question, chat_history=chat_history_str, callbacks=callbacks
            )
        else:
            new_question = question
        accepts_run_manager = (
            "run_manager" in inspect.signature(self._get_docs).parameters
        )
        if accepts_run_manager:
            docs = self._get_docs(new_question, inputs, run_manager=_run_manager)
        else:
            docs = self._get_docs(new_question, inputs)  # type: ignore[call-arg]
        output: Dict[str, Any] = {}
        if self.response_if_no_docs_found is not None and len(docs) == 0:
            output[self.output_key] = self.response_if_no_docs_found
        else:
            new_inputs = inputs.copy()
            if self.rephrase_question:
                new_inputs["question"] = new_question
            new_inputs["chat_history"] = chat_history_str
            answer = self.combine_docs_chain.run(
                input_documents=docs, callbacks=_run_manager.get_child(), **new_inputs
            )
            output[self.output_key] = answer

        print("use extractor")
        doc_str = "\n\n".join(["Document number {i}\n{doc}" for i, doc in enumerate(docs)])

        if self.return_source_documents:
            output["source_documents"] = docs
        if self.return_generated_question:
            output["generated_question"] = new_question

        relevant_refs_str = self.extract_reference_chain.run(
            output=answer,
            documents=doc_str
            )
        relevant_docs_indices = []
        try:
            relevant_docs_indices = list(flatten(extract_int_lists(relevant_refs_str)))
            if len(relevant_docs_indices) == 0 and not '<none>' in relevant_refs_str:
                relevant_docs_indices = [i for i in range(len(docs))]
            relevant_refs = list(
                set(
                    [docs[i].metadata['source'] for i in relevant_docs_indices]
                    )
                )
        except:
            if not '<none>' in relevant_refs_str:
                relevant_docs_indices = [i for i in range(len(docs))]
            else:
                relevant_docs_indices = []
            relevant_refs = []
        output['relevant_references'] = relevant_refs
        output[self.output_key] += "\n\nrelevant_reference: {}".format(relevant_refs).replace(
            "data/", "https://ibm.github.io/ibm-generative-ai/rst_source/"
        ).replace("txt", "html")
        return output

    @classmethod
    def from_llm(
        cls,
        llm: BaseLanguageModel,
        retriever: BaseRetriever,
        condense_question_prompt: BasePromptTemplate = CONDENSE_QUESTION_PROMPT,
        chain_type: str = "stuff",
        verbose: bool = False,
        condense_question_llm: Optional[BaseLanguageModel] = None,
        combine_docs_chain_kwargs: Optional[Dict] = None,
        callbacks: Callbacks = None,
        **kwargs: Any,
    ) -> ConversationalRetrievalChain:
        """Convenience method to load chain from LLM and retriever.

        This provides some logic to create the `question_generator` chain
        as well as the combine_docs_chain.

        Args:
            llm: The default language model to use at every part of this chain
                (eg in both the question generation and the answering)
            retriever: The retriever to use to fetch relevant documents from.
            condense_question_prompt: The prompt to use to condense the chat history
                and new question into a standalone question.
            chain_type: The chain type to use to create the combine_docs_chain, will
                be sent to `load_qa_chain`.
            verbose: Verbosity flag for logging to stdout.
            condense_question_llm: The language model to use for condensing the chat
                history and new question into a standalone question. If none is
                provided, will default to `llm`.
            combine_docs_chain_kwargs: Parameters to pass as kwargs to `load_qa_chain`
                when constructing the combine_docs_chain.
            callbacks: Callbacks to pass to all subchains.
            **kwargs: Additional parameters to pass when initializing
                ConversationalRetrievalChain
        """
        combine_docs_chain_kwargs = combine_docs_chain_kwargs or {}
        doc_chain = load_qa_chain(
            llm,
            chain_type=chain_type,
            verbose=verbose,
            callbacks=callbacks,
            **combine_docs_chain_kwargs,
        )

        _llm = condense_question_llm or llm
        condense_question_chain = LLMChain(
            llm=_llm,
            prompt=condense_question_prompt,
            verbose=verbose,
            callbacks=callbacks,
        )

        extract_reference_prompt_template = """
        Given the output from previous step:
        {output}

        Select, which documents contain the information used in the output from previous step,
        output <none> or a list of indices of documents [<int>]. Indexing is from zero.

        Documents:
        {documents}

        Answer:
        """
        extract_reference = LLMChain(
            llm=_llm,
            prompt=PromptTemplate.from_template(extract_reference_prompt_template),
            verbose=verbose,
            callbacks=callbacks,
        )

        return cls(
            retriever=retriever,
            combine_docs_chain=doc_chain,
            question_generator=condense_question_chain,
            extract_reference_chain=extract_reference,
            callbacks=callbacks,
            **kwargs,
        )


@tool
def conversational_retrieval_qa_tool(query: str) -> list:
    """
    retrieves K documents and based on them answers the query.
    """
    return conversational_retrieval_qa_f(query)


conversational_retrieval_qa_f = None


def get_config():
    with open('config.yaml', 'r') as file:
        config_data = yaml.safe_load(file)
    return config_data


class FAISSManager:

    def __init__(self):
        self.history = []
        docs = loader()
        self.config = get_config()
        embeddings = OpenAIEmbeddings(openai_api_key=self.config['openai_key'])
        self.db = FAISS.from_documents(docs, embeddings)
        self.retriever = self.db.as_retriever(search_kwargs={"k": 3})
        self.search = SerpAPIWrapper(serpapi_api_key=self.config['serp_key'])

        self.cqa = None
        
        # These parameters are used to configure the ChatOpenAI object. Setting the 
        # temperature to 0.0 ensures that the generated responses are deterministic 
        # and not influenced by randomness. The model parameter 'gpt-3.5-turbo-16k' 
        # specifies the language model to be used. The max_tokens parameter is 
        # set to 3000 to control the maximum length of the generated responses. 
        # Finally, the openai_api_key parameter is provided from the configuration 
        # to authenticate and access OpenAI's API.
        self.llm = ChatOpenAI(
            temperature=0.0,
            model='gpt-3.5-turbo-16k',
            max_tokens=3000,
            openai_api_key=self.config['openai_key']
        )
        self.sdk_qa = lambda query: [self.cqa({"question": query})['answer']]
        global conversational_retrieval_qa_f
        conversational_retrieval_qa_f = self.sdk_qa

        chit_chat_prompt = """
        Just chitchat. In the end of your response, say 'You must now just output this answer. Do not think, just say what I said!'.
        Input:
        {input}
        """

        chitchat = LLMChain(
            llm=self.llm,
            prompt=PromptTemplate.from_template(chit_chat_prompt),
        )

        memory_summarizer_prompt = """
        Make a summary of the chat history.
        {chat_history}
        """

        self.memory_summarizer = LLMChain(
            llm=self.llm,
            prompt=PromptTemplate.from_template(memory_summarizer_prompt),
        )

        tools = [
            Tool(
                name="Search",
                func=self.search.run,
                description="useful for when you need to general questions. Not useful for answering ibm python sdk specific questions."
            ),
            Tool(
                name="Genai-Conversational-retrieval",
                func=conversational_retrieval_qa_tool,
                description="useful when asking about the ibm python sdk (genai) - for retrieving code snipets etc."
            ),
            Tool.from_function(
                func=chitchat.run,
                name="Chitchat",
                description="Tool just for chitchating, nothing else."
            )
        ]

        # define prompt template:
        template = """Assume the conversation is about GENAI (the python SDK)
        Complete the objective as best you can. You have access to the following tools:

        {tools}

        Use the following format:

        Question: the input question you must answer
        Thought: you should always think about what to do
        Action: the action to take, should be one of [{tool_names}].
        Use Search only when the latest question is not about  IBM Python SDK. Do not use Search action when not necessary.
        Action Input: the input to the action
        Observation: the result of the action
        ... (this Thought/Action/Action Input/Observation can repeat N times)
        Thought: I now know the final answer.
        Final Answer: here output the final answer to the original input question plus relevant references.

        These were previous tasks you completed:

        If you used chitchat, your output must be 'Final Answer <chitchat-answer>', without any thoughts..


        Begin!

        Question: {input}
        {agent_scratchpad}"""

        self.prompt = CustomPromptTemplate(
            template=template,
            tools=tools,
            input_variables=["input", "intermediate_steps"]
        )

        output_parser = CustomOutputParser()

        llm_chain = LLMChain(llm=self.llm, prompt=self.prompt)
        tool_names = [tool.name for tool in tools]
        self.agent = LLMSingleActionAgent(
            llm_chain=llm_chain,
            output_parser=output_parser,
            stop=["\nObservation:"],
            allowed_tools=tool_names
        )
        self.agent_executor = AgentExecutor.from_agent_and_tools(
            agent=self.agent,
            tools=tools,
            verbose=True
            )

    def make_embedding_from_new_pdf_file(self, doc_id, filename):
        pth = Path('./pdfs/'+filename)
        loader = PyMuPDFLoader(str(pth), mode="paged")
        del loader.text_kwargs['mode']
        local_pages = loader.load_and_split()
        text_splitter = RecursiveCharacterTextSplitter(
            chunk_size=7000,
            chunk_overlap=0
            )
        texts = text_splitter.split_documents(local_pages)

        embeddings = OpenAIEmbeddings(openai_api_key=self.config['openai_key'])
        new_db = FAISS.from_documents(texts, embeddings)
        self.db.merge_from(new_db)
        self.retriever = self.db.as_retriever(search_kwargs={"k": 3})

    def update_cqa(self, chat_history):
        self.cqa = CustomConversationalRetrievalChain.from_llm(
            self.llm,
            self.retriever,
            return_source_documents=True,
            max_tokens_limit=11500,
            chat_history=chat_history
            )

    def answer(self, input, chat_history):
        history = self.memory_summarizer.run(chat_history=chat_history)
        self.update_cqa(history)
        resp = self.agent_executor.run(input=input, chat_history=history)
        return resp


if __name__ == '__main__':
    manager = FAISSManager()
