from fastapi import FastAPI, UploadFile, File, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from typing import Any, Dict
import os
import traceback
from pydantic import BaseModel
from typing import Optional
from model import FAISSManager


app = FastAPI()

allowed_origins_regex = [
    'http://localhost:8080.*',
]

app.add_middleware(
    CORSMiddleware,
    allow_origin_regex='|'.join(allowed_origins_regex),
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

ALLOWED_EXTENSIONS = {'pdf'}
manager = None


def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[-1].lower() in ALLOWED_EXTENSIONS


class FileUploadOut(BaseModel):
    answer: Optional[str] = "File uploaded with id: {}"


@app.post("/answer")
def answer(data: Dict[str, Any]):
    try:
        user_input = data['input']
        chat_history = data['chat_history']

        ans = manager.answer(user_input, chat_history)

        # Return the answer as a response
        return {'answer': ans}
    except:
        raise HTTPException(status_code=400, detail=traceback.format_exc())


@app.get("/health")
async def health():
    return {'answer': 'healthy'}


@app.post("/upload_pdf", responses={
    200: {'model': FileUploadOut, 'description': 'Successful Response'}})
async def upload_file(filename, file: UploadFile = File(...)):
    try:
        if filename == '':
            raise HTTPException(status_code=400, detail="No file selected")

        if file and allowed_file(filename):
            filename = ".".join(filename.split('.')[-2:])
            os.makedirs('pdfs/', exist_ok=True)
            file_content = await file.read()
            file_path = f"pdfs/{filename}"

            with open(file_path, 'wb') as f:
                f.write(file_content)

            doc_id = ".".join(filename.split('.')[0:-1])

            manager.make_embedding_from_new_pdf_file(doc_id, filename)
            return {
                'answer':
                    "<<p>file uploaded with id: {} with name {}</p>".format(
                        doc_id,
                        filename
                        )
                }
        else:
            raise HTTPException(status_code=400, detail="Bad file format.")
    except:
        raise HTTPException(status_code=400, detail=traceback.format_exc())


@app.on_event("startup")
async def startup_event():
    global manager
    manager = FAISSManager()


if __name__ == '__main__':
    import uvicorn
    uvicorn.run(app, host='0.0.0.0', port=8080)
